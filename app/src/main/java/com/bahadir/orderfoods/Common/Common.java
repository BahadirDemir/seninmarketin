package com.bahadir.orderfoods.Common;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.bahadir.orderfoods.Model.User;

//o anda bulunan gecerli kullaniciyi tutmak icin
public class Common {
    public static User currentUser;

    public static String convertCodeToStatus(String status) {
        if("0".equals(status))
            return"Sepetteki Urunler siraya alindi";
        else if ("1".equals(status))
            return "Urunler hazirlaniyor";
        else
            return "Urunler hazirlandi";
    }

    public static final String DELETE = "Sepetten Cikar";
    public static  boolean IsConnectedToInternet(Context context){
        ConnectivityManager connectivityManager = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if(connectivityManager!=null){
            NetworkInfo[] info = connectivityManager.getAllNetworkInfo();
            if(info !=null){
                for(int i=0;i<info.length;i++){
                    if(info[i].getState() == NetworkInfo.State.CONNECTED)
                        return true;
                }
            }
        }
        return false;
    }

}
