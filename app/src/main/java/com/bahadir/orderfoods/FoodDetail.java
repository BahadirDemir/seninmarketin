package com.bahadir.orderfoods;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bahadir.orderfoods.Common.Common;
import com.bahadir.orderfoods.Database.Database;
import com.bahadir.orderfoods.Model.Food;
import com.bahadir.orderfoods.Model.Order;
import com.cepheuen.elegantnumberbutton.view.ElegantNumberButton;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;
import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

public class FoodDetail extends AppCompatActivity {

    //xml dosyasinda tanimladigimiz ve ekranda gosterecegimiz degiskenleri tanimliyoruz.
    TextView food_name , food_price , food_description ,food_company;
    ImageView food_image;
    CollapsingToolbarLayout collapsingToolbarLayout;
    FloatingActionButton btnCart;
    ElegantNumberButton numberButton;

    String foodId="";
    String foodbarcode="";

    FirebaseDatabase database;
    DatabaseReference foods;

    Food currentFood;

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                .setDefaultFontPath("fonts/cf.otf")
                .setFontAttrId(R.attr.fontPath)
                .build());
        setContentView(R.layout.activity_food_detail);

        //Firebase
        database = FirebaseDatabase.getInstance();
        foods = database.getReference("Food");

        //Init view
        numberButton = (ElegantNumberButton)findViewById(R.id.number_button);
        btnCart = (FloatingActionButton)findViewById(R.id.btnCart);

        btnCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new Database(getBaseContext()).addToCart(new Order(
                        foodId,
                        currentFood.getName(),
                        numberButton.getNumber(),
                        currentFood.getPrice(),
                        currentFood.getDiscount(),
                        currentFood.getImage()
                ));
                Toast.makeText(FoodDetail.this,"Siparise Eklendi",Toast.LENGTH_SHORT).show();
            }
        });

        food_description = (TextView)findViewById(R.id.food_description);
        food_price= (TextView)findViewById(R.id.food_price);
        food_name=(TextView)findViewById(R.id.food_name);
        food_image=(ImageView)findViewById(R.id.food_image);
        food_company=(TextView)findViewById(R.id.food_company);

        collapsingToolbarLayout = (CollapsingToolbarLayout)findViewById(R.id.collapsing);
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppbar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppbar);

        //food id yi alma
        if(getIntent() !=null) {
            foodId = getIntent().getStringExtra("FoodId");
            foodbarcode =getIntent().getStringExtra("Barcode");
        }
        if(!(foodId==null))
        {
            if(Common.IsConnectedToInternet(getBaseContext()))
            getDetailFood(foodId);
            else {
                Toast.makeText(FoodDetail.this, "Lütfen İnternet Bağlantınızı Kontrol Edin ", Toast.LENGTH_SHORT).show();
                return;
            }
        }
        if(!(foodbarcode==null)){
            if(Common.IsConnectedToInternet(getBaseContext()))
            getDetailFood2(foodbarcode);
            else {
                Toast.makeText(FoodDetail.this, "Lütfen İnternet Bağlantınızı Kontrol Edin ", Toast.LENGTH_SHORT).show();
                return;
            }
        }
    }

    private void getDetailFood2(String foodbarcode) {
        Query query =foods.orderByChild("barcode").equalTo(foodbarcode);
            query.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                    if(dataSnapshot.getChildrenCount()>0){
                        for(DataSnapshot child:dataSnapshot.getChildren()){
                            currentFood=child.getValue(Food.class);

                            //SetImage
                            Picasso.with(getBaseContext()).load(currentFood.getImage())
                                    .into(food_image);

                            collapsingToolbarLayout.setTitle(currentFood.getName());

                            food_price.setText(String.format("%s TL" , currentFood.getPrice()));

                            food_name.setText(currentFood.getName());

                            food_description.setText(currentFood.getDescription());

                            food_company.setText(currentFood.getCompany());
                        }
                    }
                }

                @Override
                public void onCancelled(@NonNull DatabaseError databaseError) {

                }
            });
    }

    private void getDetailFood(String foodId) {
        foods.child(foodId).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                currentFood=dataSnapshot.getValue(Food.class);

                //SetImage
                Picasso.with(getBaseContext()).load(currentFood.getImage())
                        .into(food_image);

                collapsingToolbarLayout.setTitle(currentFood.getName());

                food_price.setText(String.format("%s TL" , currentFood.getPrice()));

                food_name.setText(currentFood.getName());

                food_description.setText(currentFood.getDescription());

                food_company.setText(currentFood.getCompany());

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }
}
