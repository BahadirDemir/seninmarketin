package com.bahadir.orderfoods.Interface;

import android.view.View;

//Recycler view de iteme tiklandiginda aktivite calismasi icin bir interface yaziyoruz
public interface ItemClickListener {
    void onClick(View view , int position , boolean IsLongClick);
}
